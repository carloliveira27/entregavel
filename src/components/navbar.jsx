import "./navbar.css"

function NavBar(){
    return(
        <div className = "navBar">
            <h1 className = "logo1">Sugar Rush</h1>
            <h1 className = "logo2"> <i>Home</i> | <i>Carrinho </i> </h1>
        </div>
    )
}

export default NavBar;