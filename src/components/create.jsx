import React, { useState } from 'react';
import './create.css';

var listaProdutos = [];

function Colocar({ onAddProduct }) {
  const [produto, setProduto] = useState({
    nome: '',
    preco: '',
    descricao: ''
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setProduto({ ...produto, [name]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    listaProdutos.push(produto);
    setProduto({ nome: '', preco: '', descricao: '' });
    onAddProduct(produto);
  };

  return (
    <div className="colocar">
      <form onSubmit={handleSubmit}>
        <div className="nome">
          <label for="nome" >Nome do Produto
          <input 
            id="nome"
            type="text"
            name="nome"
            value={produto.nome}
            onChange={handleChange} />
            </label>
        </div>
        <div className="nome">
          <label >Preço
          <input 
            id="preco"
            type="text"
            name="preco"
            value={produto.preco}
            onChange={handleChange} />
            </label>
        </div>
        <div className="nome">
          <label >Descrição
          <input 
            id="descricao"
            type="text"
            name="descricao"
            value={produto.descricao}
            onChange={handleChange} />
            </label>
        </div>
        <button type="submit">
          <div className="bola">
            <img src="../mais.png" className="mais" alt="Adicionar" />
          </div>
        </button>
      </form>
    </div>
  );
}

function Create({ onAddProduct }) {
  return (
    <div className="create">
      <Colocar onAddProduct={onAddProduct} />
    </div>
  );
}

export default Create;
