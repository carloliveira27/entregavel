import React from 'react';
import { useParams } from 'react-router-dom';
import NavBar from "./navbar.jsx";
import Item from './item.jsx';
import "./carrinho.css";

const Carrinho = () => {
  const { name, preco, descricao } = useParams();

  return (
    <>
      <NavBar />
      <div className="caixas">
        <div className="caixa1">
            <div className="icone">
          <Item name={name} preco={preco} descricao={descricao} className="icone" />
          </div>
          <h1 className="texto1">{preco}</h1>
        </div>
        <div className="caixa2">
          <h1 className="texto2">{descricao}</h1>
          <div className = "puts">
          <input type="number" className = "butao"/>
          <button type="submit">
          <div className="bola">
            <img src="../mais.png" className="mais" alt="Adicionar" />
          </div>
        </button>
        </div>
        </div>
      </div>
    </>
  );
}

export default Carrinho;
