import "./barra.css"
import Item from "./item.jsx"
import "./create.jsx"

function Barra({ produtos }) {
    return (
      <div className="barra">
        {produtos.map((produto, index) => (
          <div key={index}>
            <Item name={produto.nome} preco = {produto.preco} descricao = {produto.descricao} />
          </div>
        ))}
      </div>
    );
  }

export default Barra;