import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import NavBar from './components/navbar.jsx';
import './components/navbar.css';
import Landing from './components/landing.jsx';
import './components/landing.css';
import Barra from './components/barra.jsx';
import './components/barra.css';
import Create from './components/create.jsx';
import './components/create.css';
import Carrinho from './components/carrinho.jsx';

function App() {
  const [produtos, setProdutos] = useState([]);

  const addProduct = (produto) => {
    setProdutos([...produtos, produto]);
  };

  return (
    <Router>
      <Routes>
        <Route 
          path="/" 
          element={
            <>
              <NavBar />
              <Landing />
              <Barra produtos={produtos} />
              <Create onAddProduct={addProduct} />
            </>
          } 
        />
        <Route path="/carrinho/:name/:preco/:descricao" element={<Carrinho />} />
      </Routes>
    </Router>
  );
}

export default App;
